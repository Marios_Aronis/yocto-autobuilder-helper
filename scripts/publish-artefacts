#!/bin/bash
WORKDIR=$1
DEST=$2
target=$3

sha256sums(){
    dest=$1
    for x in `find -L $dest -maxdepth 5 -type f`; do
        for w in $x;do
            if [ ${w##*.} != sha256sum ]; then
                shasum=`sha256sum $w | awk '{print $1}'`
                filename=${w##*/}
                echo $shasum $filename >> $w.sha256sum
            fi
        done
    done
}

TMPDIR=$WORKDIR/tmp

case "$target" in
    "qemuarm"|"qemuarm64"|"qemumips64"|"qemumips"|"qemuppc"|"qemux86"|"qemux86-64"|"edgerouter"|"mpc8315e-rdb"|"genericx86"|"genericx86-64"|"beaglebone"|"ds-cm3"|"ds-stratocan"|"ds-sdk"|"ds-qemux86-64")
        ### mkdir -p $DEST/toolchain/i686
        if [ -n "`ls -l $TMPDIR/deploy/sdk/*x86_64-core-image* 2>/dev/null`" ];then
        	mkdir -p $DEST/toolchain/x86_64
        	sha256sums $TMPDIR/deploy/sdk
        	cp -R --no-dereference --preserve=links $TMPDIR/deploy/sdk/*x86_64-core-image* $DEST/toolchain/x86_64
	fi
        ;;
esac

case "$target" in
    "build-appliance")
        mkdir -p $DEST/build-appliance
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/qemux86-64/*.zip $DEST/build-appliance
        ;;
    "buildtools")
        mkdir -p $DEST/buildtools
        ### mkdir -p $DEST/toolchain/i686
        mkdir -p $DEST/toolchain/x86_64
        ### mkdir -p $DEST/toolchain/aarch64
        sha256sums $TMPDIR/deploy/sdk
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/sdk/*buildtools* $DEST/buildtools
        ### cp -R --no-dereference --preserve=links $TMPDIR/deploy/sdk/i686-nativesdk-libc* $DEST/toolchain/i686
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/sdk/x86_64-nativesdk-libc* $DEST/toolchain/x86_64
        ### cp -R --no-dereference --preserve=links $TMPDIR/deploy/sdk/aarch64-nativesdk-libc* $DEST/toolchain/aarch64
        ;;
    "qemuarm64")
        mkdir -p $DEST/machines/qemu/qemuarm64
        sha256sums $TMPDIR/deploy/images/qemuarm64
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/qemuarm64/*qemuarm64* $DEST/machines/qemu/qemuarm64
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/qemuarm64/*Image* $DEST/machines/qemu/qemuarm64
        ;;
    "qemuarm")
        mkdir -p $DEST/machines/qemu/qemuarm
        sha256sums $TMPDIR/deploy/images/qemuarm
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/qemuarm/*qemuarm* $DEST/machines/qemu/qemuarm
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/qemuarm/*Image* $DEST/machines/qemu/qemuarm
        ;;
    "beaglebone")
        mkdir -p $DEST/machines/beaglebone-yocto
        sha256sums $TMPDIR/deploy/images/beaglebone-yocto
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/beaglebone-yocto/*Image* $DEST/machines/beaglebone-yocto
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/beaglebone-yocto/*u-boot* $DEST/machines/beaglebone-yocto
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/beaglebone-yocto/*beaglebone-yocto* $DEST/machines/beaglebone-yocto
        ;;
    "qemuarm-alt")
        mkdir -p $DEST/machines/qemu/qemuarm-lsb
        sha256sums $TMPDIR/deploy/images/qemuarm
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/qemuarm/*qemuarm* $DEST/machines/qemu/qemuarm-lsb
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/qemuarm/*Image* $DEST/machines/qemu/qemuarm-lsb
        ;;
    "beaglebone-alt")
        mkdir -p $DEST/machines/beaglebone-yocto-lsb
        sha256sums $TMPDIR/deploy/images/beaglebone-yocto
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/beaglebone-yocto/*Image* $DEST/machines/beaglebone-yocto-lsb
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/beaglebone-yocto/*u-boot* $DEST/machines/beaglebone-yocto-lsb
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/beaglebone-yocto/*beaglebone-yocto* $DEST/machines/beaglebone-yocto-lsb
        ;;
    "qemumips64")
        mkdir -p $DEST/machines/qemu/qemumips64
        sha256sums $TMPDIR/deploy/images/qemumips64
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/qemumips64/*qemumips64* $DEST/machines/qemu/qemumips64
        ;;
    "qemumips")
        mkdir -p $DEST/machines/qemu/qemumips
        sha256sums $TMPDIR/deploy/images/qemumips
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/qemumips/*qemumips* $DEST/machines/qemu/qemumips
        ;;
    "edgerouter")
        mkdir -p $DEST/machines/edgerouter
        sha256sums $TMPDIR/deploy/images/edgerouter
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/edgerouter/*edgerouter* $DEST/machines/edgerouter
        ;;
    "qemumips-alt")
        mkdir -p $DEST/machines/qemu/qemumips-lsb
        sha256sums $TMPDIR/deploy/images/qemumips
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/qemumips/*qemumips* $DEST/machines/qemu/qemumips-lsb
        ;;
    "edgerouter-alt")
        mkdir -p $DEST/machines/edgerouter-lsb
        sha256sums $TMPDIR/deploy/images/edgerouter
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/edgerouter/*edgerouter* $DEST/machines/edgerouter-lsb
        ;;
    "qemuppc")
        mkdir -p $DEST/machines/qemu/qemuppc
        sha256sums $TMPDIR/deploy/images/qemuppc
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/qemuppc/*qemuppc* $DEST/machines/qemu/qemuppc
        ;;
    "mpc8315e-rdb")
        mkdir -p $DEST/machines/mpc8315e-rdb
        sha256sums $TMPDIR/deploy/images/mpc8315e-rdb
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/mpc8315e-rdb/*mpc8315* $DEST/machines/mpc8315e-rdb
        ;;
    "qemuppc-alt")
        mkdir -p $DEST/machines/qemu/qemuppc-lsb
        sha256sums $TMPDIR/deploy/images/qemuppc
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/qemuppc/*qemuppc* $DEST/machines/qemu/qemuppc-lsb
        ;;
    "mpc8315e-rdb-alt")
        mkdir -p $DEST/machines/mpc8315e-rdb-lsb
        sha256sums $TMPDIR/deploy/images/mpc8315e-rdb
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/mpc8315e-rdb/*mpc8315* $DEST/machines/mpc8315e-rdb-lsb
        ;;
    "ds-cm3")
        mkdir -p $DEST/machines/ds-cm3
        sha256sums $TMPDIR/deploy/images/ds-cm3
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/ds-cm3/*Image* $DEST/machines/ds-cm3
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/ds-cm3/*u-boot* $DEST/machines/ds-cm3
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/ds-cm3/*ds-cm3* $DEST/machines/ds-cm3
        ;;
    "ds-stratocan")
        mkdir -p $DEST/machines/ds-stratocan
        sha256sums $TMPDIR/deploy/images/ds-stratocan
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/ds-stratocan/*Image* $DEST/machines/ds-stratocan
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/ds-stratocan/*u-boot* $DEST/machines/ds-stratocan
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/ds-stratocan/*ds-stratocan* $DEST/machines/ds-stratocan
        ;;
    "ds-qemux86-64")
        mkdir -p $DEST/machines/qemu/qemux86-64
        sha256sums $TMPDIR/deploy/images/qemux86-64
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/qemux86-64/*qemux86-64* $DEST/machines/qemu/qemux86-64
        ;;
    "ds-os")
        mkdir -p $DEST/repo
        sha256sums $TMPDIR/deploy/deb
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/deb $DEST/repo
        ### cp -R --no-dereference --preserve=links $TMPDIR/deploy/rpm $DEST/repo
        ;;
    "wic")
        mkdir -p $DEST/machines/qemu/qemux86
        sha256sums $TMPDIR/deploy/wic_images/qemux86
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/wic_images/qemux86/*/*/*.direct $DEST/machines/qemu/qemux86
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/wic_images/qemux86/*/*/*.direct.sha256sum $DEST/machines/qemu/qemux86
        mkdir -p $DEST/machines/genericx86
        sha256sums $TMPDIR/deploy/wic_images/genericx86
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/wic_images/genericx86/*/*/*.direct $DEST/machines/genericx86
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/wic_images/genericx86/*/*/*.direct.sha256sum $DEST/machines/genericx86
        mkdir -p $DEST/machines/qemu/qemux86-64
        sha256sums $TMPDIR/deploy/wic_images/qemux86-64
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/wic_images/qemux86-64/*/*/*.direct $DEST/machines/qemu/qemux86-64
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/wic_images/qemux86-64/*/*/*.direct.sha256sum $DEST/machines/qemu/qemux86-64
        mkdir -p $DEST/machines/genericx86-64
        sha256sums $TMPDIR/deploy/wic_images/genericx86-64
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/wic_images/genericx86-64/*/*/*.direct $DEST/machines/genericx86-64
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/wic_images/genericx86-64/*/*/*.direct.sha256sum $DEST/machines/genericx86-64
        ;;
    "qemux86-64")
        mkdir -p $DEST/machines/qemu/qemux86-64
        sha256sums $TMPDIR/deploy/images/qemux86-64
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/qemux86-64/*qemux86-64* $DEST/machines/qemu/qemux86-64
        ;;
    "genericx86-64")
        mkdir -p $DEST/machines/genericx86-64
        sha256sums $TMPDIR/deploy/images/genericx86-64
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/genericx86-64/*genericx86-64* $DEST/machines/genericx86-64
        ;;
    "qemux86-64-alt")
        mkdir -p $DEST/machines/qemu/qemux86-64-lsb
        sha256sums $TMPDIR/deploy/images/qemux86-64
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/qemux86-64/*qemux86-64* $DEST/machines/qemu/qemux86-64-lsb
        ;;
    "genericx86-64-alt")
        mkdir -p $DEST/machines/genericx86-64-lsb
        sha256sums $TMPDIR/deploy/images/genericx86-64
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/genericx86-64/*genericx86-64* $DEST/machines/genericx86-64-lsb
        ;;
    "nightly-x86-64-bsp")
        rm -rf $DEST/$target/images/intel-corei7-64/*
        mkdir -p $DEST/$target/images/intel-corei7-64
        sha256sums $TMPDIR/deploy/images/intel-corei7-64
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/intel-corei7-64/bzImage* $DEST/$target/images/intel-corei7-64
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/intel-corei7-64/*core-image-sato-sdk-intel-corei7-64*tar* $DEST/$target/images/intel-corei7-64
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/intel-corei7-64/*modules-* $DEST/$target/images/intel-corei7-64
        ;;
    *-"qemux86")
        mkdir -p $DEST/machines/qemu/qemux86
        sha256sums $TMPDIR/deploy/images/qemux86
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/qemux86/*qemux86* $DEST/machines/qemu/qemux86
        ;;
    "genericx86")
        mkdir -p $DEST/machines/genericx86
        sha256sums $TMPDIR/deploy/images/genericx86
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/genericx86/*genericx86* $DEST/machines/genericx86
        ;;
    "qemux86-alt")
        mkdir -p $DEST/machines/qemu/qemux86-lsb
        sha256sums $TMPDIR/deploy/images/qemux86
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/qemux86/*qemux86* $DEST/machines/qemu/qemux86-lsb
        ;;
    "genericx86-alt")
        mkdir -p $DEST/machines/genericx86-lsb
        sha256sums $TMPDIR/deploy/images/genericx86
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/genericx86/*genericx86* $DEST/machines/genericx86-lsb
        ;;
    "poky-tiny")
        mkdir -p $DEST/machines/qemu/qemu-tiny
        sha256sums $TMPDIR/deploy/images/qemux86
        cp -R --no-dereference --preserve=links $TMPDIR/deploy/images/qemux86/*qemux86* $DEST/machines/qemu/qemu-tiny
        ;;
    "eclipse-plugin-neon")
        DIR=$WORKDIR/../scripts
        # create sha256sums only for the zip files
        for x in `ls $DIR/*.zip`; do sha256sum $x >> $x.sha256sum; done
        mkdir -p $DEST/eclipse-plugin/neon
        cp --no-dereference --preserve=links $DIR/org.*.zip $DIR/org.*.sha256sum $DEST/eclipse-plugin/neon
        ;;
    "eclipse-plugin-oxygen")
        DIR=$WORKDIR/../scripts
        # create sha256sums only for the zip files
        for x in `ls $DIR/*.zip`; do sha256sum $x >> $x.sha256sum; done
        mkdir -p $DEST/eclipse-plugin/oxygen
        cp --no-dereference --preserve=links $DIR/org.*.zip $DIR/org.*.sha256sum $DEST/eclipse-plugin/oxygen
        ;;
esac

